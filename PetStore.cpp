// PetStore.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

using namespace std;

void mainmenu();

//main function
int main()
{
    mainmenu();

}

void mainmenu()
{
    cout << "\n\n";
    cout << "************* STOCK MANAGEMENT SYSTEM *************\n";
    cout << "***************************************************\n";
    cout << "**********  1-  Sell items            *************\n";
    cout << "**********  2-  Restock items         *************\n";
    cout << "**********  3-  Add new items         *************\n";
    cout << "**********  4-  Update stock qty      *************\n";
    cout << "**********  5-  list all products     *************\n";
    cout << "**********  6-  View report of sales  *************\n";
    cout << "**********  7-  Load Data from file   *************\n";
    cout << "**********  8-  Exit the program      *************\n";
    cout << "***************************************************\n";
    cout << "Enter your choice : ";
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
